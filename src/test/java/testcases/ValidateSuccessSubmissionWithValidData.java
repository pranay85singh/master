package testcases;


import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import pageobjects.*;
import test.core.BaseTest;
import test.core.TestUtil;


public class ValidateSuccessSubmissionWithValidData extends BaseTest
{
		 	
		@Test
		public void testSuccessSubmissionWithValidData()
		{
		
				try 
				{
					
					TestUtil.reportLog("****   Validate a user can submit a contact ifo successfully. ****");
					
					JupiterHomePage jh = new JupiterHomePage(getDriver());
					JupiterContactPage jc = new JupiterContactPage(getDriver());
					ReportPage rp 		= new ReportPage(getDriver());

					if(jh.isHomePageDisplayed())
						TestUtil.reportLog("VP 1 : ----> Home Page is displayed successfully");
					else
						Assert.fail("Home page is NOT displayed.");

					jh.clickcontactLinkItem();
					jc.enterforenameTextBox("Pranay");
					jc.enterSurnameTextBox("Singh");
					jc.enterEmailTextBox("pranay85singh@gmail.com");
					jc.enterPhoneTextBox("02101111111");
					jc.enterMessageTextBox("Hello Testing");
					jc.clickSubmitTextBox();

					if(rp.isCorrectSuccessMessageDisplayed())
						TestUtil.reportLog("VP 2 : ----> Request submitted successfully");
					else
						Assert.fail("Request is not submitted.");


					
				}
				catch (Exception ex) 
				{
					ex.printStackTrace();
					AssertJUnit.fail("Threw exception: " + ex.getMessage() + " " + ex.getStackTrace().toString());
				}	
		

		}
	
	
	
}
