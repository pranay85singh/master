package testcases;

import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import pageobjects.*;
import test.core.BaseTest;
import test.core.TestUtil;

public class ValidateCartWithItems extends BaseTest {

    @Test
    public void testCartWithItems()
    {

        try
        {

            TestUtil.reportLog("****   Validate a user can add Items into cart. ****");

            JupiterHomePage jh = new JupiterHomePage(getDriver());
            JupiterShopPage js = new JupiterShopPage(getDriver());
            ReportPage rp 		= new ReportPage(getDriver());

            if(jh.isHomePageDisplayed())
                TestUtil.reportLog("VP 1 : ----> Home Page is displayed successfully");
            else
                Assert.fail("Home page is NOT displayed.");

            js.clickShopLinkItem();
            js.addProductToCart("Funny Cow");
            js.addProductToCart("Funny Cow");
            js.addProductToCart("Fluffy Bunny");
            js.clickToCart();


        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            AssertJUnit.fail("Threw exception: " + ex.getMessage() + " " + ex.getStackTrace().toString());
        }


    }
}
