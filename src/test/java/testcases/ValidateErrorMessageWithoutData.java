package testcases;


import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import pageobjects.*;
import test.core.BaseTest;
import test.core.TestUtil;


public class ValidateErrorMessageWithoutData extends BaseTest
{
		 	
		@Test
		public void testErrorWithValidData()
		{

			try
			{

				TestUtil.reportLog("****   Validate a user can not submit a request with filling mandatory contact ifo. ****");

				JupiterHomePage jh = new JupiterHomePage(getDriver());
				JupiterContactPage jc = new JupiterContactPage(getDriver());
				ReportPage rp 		= new ReportPage(getDriver());

				if(jh.isHomePageDisplayed())
					TestUtil.reportLog("VP 1 : ----> Home Page is displayed successfully");
				else
					Assert.fail("Home page is NOT displayed.");

				jh.clickcontactLinkItem();
				jc.clickSubmitTextBox();

				if(rp.isErrorMessageDisplayed())
					TestUtil.reportLog("VP 2 : ----> Valid errors are displayed successfully");
				else
					Assert.fail("Home page is NOT displayed.");

				jc.enterforenameTextBox("Pranay");
				jc.enterEmailTextBox("pranay85singh@gmail.com");
				jc.enterMessageTextBox("Hello Testing");

				if(rp.isErrorMessageGone())
					TestUtil.reportLog("VP 3 : ----> Valid errors are gone successfully");
				else
					Assert.fail("Conatct page is NOT displayed.");


			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				AssertJUnit.fail("Threw exception: " + ex.getMessage() + " " + ex.getStackTrace().toString());
			}
		

		}
	
	
	
}
