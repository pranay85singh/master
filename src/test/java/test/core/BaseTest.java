package test.core;



import io.github.bonigarcia.wdm.WebDriverManager;
import io.restassured.response.Response;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
















































import java.util.logging.Level;







//import junit.framework.Assert;
//import org.junit.Test;
//import static org.junit.Assert.*;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;














import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;

import org.json.JSONException;
import org.json.JSONObject;
//import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ThreadGuard;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.slf4j.event.Level;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;







public abstract class BaseTest 
{
	
	private String _appURL;
	protected String userProfile = "";
	protected String browserName = "";
	private static WebDriver driver;
	private static String osName = "";
	public String nodeURL;
	public String accessToken = "";
	public String ROOT_URI = "";	
	public String accessTokenFile = ""; 
	

	
		   
    @BeforeTest 
    @Parameters({"initialURL","profile","browserName"})
    public void beforeSuiteConfig(
		String initURL,
		String profile,
		String browser) throws MalformedURLException
    {	
        _appURL 	= 	initURL;
        userProfile	=	profile;
        browserName = 	System.getenv("browserName");
        osName		= 	System.getProperty("os.name").toLowerCase();
        
        
        DateFormat dateFormat = new SimpleDateFormat("yy-mm-dd HH-mm-ss");
        Date date = new Date();

    	
        if(browserName.equalsIgnoreCase("firefox"))
		{      
        	         	        	
        	WebDriverManager.firefoxdriver().setup();
        	
        	FirefoxOptions options = new FirefoxOptions();
      	            	
        	options.setCapability("acceptSslCerts", true);
        	options.addArguments("--headless");
        	options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
        	options.setLogLevel(FirefoxDriverLogLevel.ERROR);
        	//options.setCapability("browser.privatebrowsing.autostart", true);
        	       	
        	driver = ThreadGuard.protect(new FirefoxDriver(options));          	
        	
        	System.out.println("We are using Firefox drivers...");
        	
        	driver.get(_appURL);
        	driver.manage().window().maximize();
        	
		}
        if(browserName.equalsIgnoreCase("chrome"))
        {          	
        	
        	WebDriverManager.chromedriver().setup();
        	       	        	                 					
        	ChromeOptions options = new ChromeOptions();
        	   
        	options.addArguments("--no-sandbox");
        	options.addArguments("--log-level=3");
        	options.addArguments("--silent");                   	        	
        	options.addArguments("--incognito");
        	options.addArguments("--start-maximized"); 
//        	options.addArguments("--headless");
        	options.addArguments("--disable-gpu");
        	options.addArguments("--window-size=1920,1080");
        	
        	options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
        	
        	     	       	       	      	
        	driver = ThreadGuard.protect(new ChromeDriver(options));       	       	      	
             	        	
        	System.out.println("We are using Chrome drivers...");           	
        	
        	driver.get(_appURL);        	    	       	      	        	        	
        	      	      	
        }
        if(browserName.equalsIgnoreCase("edge"))
        {          	       	
        	
        	WebDriverManager.edgedriver().setup();
        	      	           	      	
        	EdgeOptions edgeOptions = new EdgeOptions();
        	

        	driver = new EdgeDriver(edgeOptions);       	       	      	
             	        	
        	System.out.println("We are using Edge drivers...");           	
        	
        	driver.get(_appURL);        	    	       	      	        	        	
        	      	      	
        }
        	          	                       
    }
    
    public static WebDriver getDriver() 
    {
        return driver;
    }


	@AfterTest
    public void close()
    {
    	
    	//We only use driver.quit if we are running a UI test. If accessToken is not empty then this implies we are running an api test.
    	if(accessToken.isEmpty()) 	
    		driver.quit();
    	   	
    }


}
	
	


