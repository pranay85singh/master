package pageobjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobject.core.BasePageObject;
import pageobject.core.FailedPreconditionException;


public class JupiterShopPage extends BasePageObject
{



	//--------------------------Add new homepage elements--------------------------------------------------------------




	private static final String LOCATOR_FORENAME_TEXT = "//input[@type='text' and @name='forename']";
	@FindBy(xpath=LOCATOR_FORENAME_TEXT)
	private WebElement forenameTextBox;

	private static final String LOCATOR_SURNAME_TEXT = "//input[@type='text' and @name='surname']";
	@FindBy(xpath=LOCATOR_SURNAME_TEXT)
	private WebElement surnameTextBox;

	private static final String LOCATOR_EMAIL_TEXT = "//input[@type='email' and @name='email']";
	@FindBy(xpath=LOCATOR_EMAIL_TEXT)
	private WebElement emailTextBox;

	private static final String LOCATOR_TELEPHONE_NUMBER = "//input[@type='tel' and @name='telephone']";
	@FindBy(xpath=LOCATOR_TELEPHONE_NUMBER)
	private WebElement telephoneTextBox;

	private static final String LOCATOR_MESSAGE_TEXT = "//textarea[@type='text' and @name='message']";
	@FindBy(xpath=LOCATOR_MESSAGE_TEXT)
	private WebElement messageTextBox;

	private static final String LOCATOR_SUBMIT_BUTTON = ".//a[contains(text(),'Submit')]";
	@FindBy(xpath=LOCATOR_SUBMIT_BUTTON)
	private WebElement submitButtonItem;

	private static final String LOCATOR_SUCCESS_MESSAGE = ".//div[starts-with(@class,'alert')]";
	@FindBy(xpath=LOCATOR_SUCCESS_MESSAGE)
	private WebElement successMessageItem;


	private static final String LOCATOR_SHOP_LINK = "//a[contains(text(),'Shop')]";
	@FindBy(xpath=LOCATOR_SHOP_LINK)
	private WebElement shopLinkItem;

	private static final String LOCATOR_CART_LINK = "//a[contains(text(),'Cart')]";
	@FindBy(xpath=LOCATOR_CART_LINK)
	private WebElement cartLinkItem;

	private static final String LOCATOR_FUNNYCOW_PRODUCT = "//*[@id=\"product-6\"]/div/p/a";
	@FindBy(xpath=LOCATOR_FUNNYCOW_PRODUCT)
	private WebElement funnyCowProdutItem;


	private static final String LOCATOR_FLUFFYBUNNY_PRODUCT = "//*[@id=\"product-4\"]/div/p/a";
	@FindBy(xpath=LOCATOR_FLUFFYBUNNY_PRODUCT)
	private WebElement fluffyBunnyProdutItem;





	//---------------------------------------------------------------------------------------------------------------------


	private WebElement pageTitle;



	public JupiterShopPage(WebDriver driver)
			throws FailedPreconditionException {
		// TODO Auto-generated constructor stub
		super(driver);
	}

	//@Override
	public boolean onCorrectPage() 
	{
		// Enter Account Information
		
		try 
		{
			return true;
		}
		catch (Exception e) 
		{
			return false;
		}
		
	}


	public void clickShopLinkItem()
	{
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_SHOP_LINK)));

		shopLinkItem.click();

	}

	public void addProductToCart(String Product)
	{
		if (Product == "Funny Cow") {

			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_FUNNYCOW_PRODUCT)));
			funnyCowProdutItem.click();

		}else if	(Product == "Fluffy Bunny"){

			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_FLUFFYBUNNY_PRODUCT)));
			fluffyBunnyProdutItem.click();
		}



	}

	public void clickToCart() {
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_CART_LINK)));
		cartLinkItem.click();

	}


}

