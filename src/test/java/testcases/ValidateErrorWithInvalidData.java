package testcases;

import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import pageobjects.*;
import test.core.BaseTest;
import test.core.TestUtil;

public class ValidateErrorWithInvalidData extends BaseTest {

    @Test
    public void testErrorMessageWithInvaliddata()
    {

        try
        {

            TestUtil.reportLog("****   Validate a user can not submit a request with filling mandatory contact ifo. ****");

            JupiterHomePage jh = new JupiterHomePage(getDriver());
            JupiterContactPage jc = new JupiterContactPage(getDriver());
            ReportPage rp 		= new ReportPage(getDriver());



            if(jh.isHomePageDisplayed())
                TestUtil.reportLog("VP 1 : ----> Home Page is displayed successfully");
            else
                Assert.fail("Home page is NOT displayed.");

            jh.clickcontactLinkItem();
            jc.enterforenameTextBox("Pranay");
            jc.enterEmailTextBox("ErrorMail");
            jc.enterPhoneTextBox("vkefvknefvkenf");
            jc.enterMessageTextBox("Hello Testing");


            if(rp.isErrorForInvalidFormatDisplayed())
                TestUtil.reportLog("VP 2 : ----> Valid errors are displayed successfully");
            else
                Assert.fail("Home page is NOT displayed.");


        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            AssertJUnit.fail("Threw exception: " + ex.getMessage() + " " + ex.getStackTrace().toString());
        }


    }
}


