#(Jupiter Web App Automation) 


To clone the repository:
```
git clone https://pranay85singh@bitbucket.org/pranay85singh/master.git
```
## Executing the tests
To run the Test as  `mvn project` from the command line.

By default, the tests will run using Chrome. 
```json
$ mvn -DbrowserName=Chrome test
      
      
      

```

The test results will be recorded in the `/target/surefire-reports` directory.


