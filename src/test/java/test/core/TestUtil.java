package test.core;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Reporter;

public class TestUtil extends BaseTest
{

	public static void takeScreenshot(String testName, String testStatus)
    {
		//TODO - Add this path to config file.
		String screenshotBasePath = "target/surefire-reports/screenshots/";
    	String fileName = testName + "_" + testStatus + "_screenshot.jpg";
    	
    	//To enable a clickable html link to be added within the report.
    	System.setProperty("org.uncommons.reportng.escape-output", "false");
    	
    	File scrFile = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.FILE);

    	String currentDirectory = System.getProperty("user.dir");
    	
    	String screenshotPath = screenshotBasePath + fileName;
    	
    	File destinationFile = new File(screenshotPath);
    	
    	    	
		try 
		{
			FileUtils.copyFile(scrFile, destinationFile);
						
			Reporter.log("<br> <a href='" + "../screenshots/" + fileName +"'> <img src='"+ "../screenshots/" + fileName +"' height='200' width='200' /> </a><br>");
	        
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    } 
	
	public static void reportLog(String reportMessage)
	{
		Reporter.log(reportMessage);	
		Reporter.log("</br></br>");
	}

}
