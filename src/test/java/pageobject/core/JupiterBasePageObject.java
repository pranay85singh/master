package pageobject.core;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.util.List;
import java.util.concurrent.TimeUnit;


public abstract class JupiterBasePageObject
{

	// By default, wait for 5 seconds for items to become visible...
	private int _waitTimeout = 5;

	protected static final int driverWaitInSec = 30;

	//------------------COMMON ELEMENTS USED ACROSS MULTIPLE PAGES------------------------------------------


	protected static final String LOCATOR_MENU_BUTTON = "//button[contains(@class,'menu-button')]";
	@FindBy(xpath=LOCATOR_MENU_BUTTON)
	protected WebElement menuButton;


	protected static final String LOCATOR_SAVE_BUTTON = "//button[contains(@class,'btn btn-success')]";
	@FindBy(xpath=LOCATOR_SAVE_BUTTON)
	protected WebElement saveButton;

	protected static final String LOCATOR_CANCEL_BUTTON = "btn btn-warning";
	@FindBy(className=LOCATOR_CANCEL_BUTTON)
	protected WebElement cancelButton;


	protected static final String LOCATOR_OK_BUTTON = "//button[text() = 'OK']";
	@FindBy(xpath=LOCATOR_OK_BUTTON)
	protected WebElement okButton;

	protected static final String LOCATOR_SEARCH_FIELD = "//input[@type='search']";
	@FindBy(xpath=LOCATOR_SEARCH_FIELD)
	protected WebElement searchField;

	protected static final String LOCATOR_DELETE_BUTTON = "//*[contains(@class, 'delete') and contains(text(),'Delete')]";
	@FindBy(xpath=LOCATOR_DELETE_BUTTON)
	protected WebElement deleteButton;


	protected static final String LOCATOR_BULKMAINTENANCE_MENUITEM = "//span[text() = 'Bulk Maintenance']";

	protected static final String LOCATOR_CLOSE_ALERT_MESSAGE = "//button[@class='close']";
	@FindBy(xpath=LOCATOR_CLOSE_ALERT_MESSAGE)
	protected WebElement closeAlertMessage;


	//--------------------------AZURE LOGIN ELEMENTS-----------------------------------------------------

	private static final String LOCATOR_SIGNIN_EMAIL_FIELD = "i0116";

	private static final String LOCATOR_SIGNIN_PASSWORD_FIELD = "i0118";

	private static final String LOCATOR_SIGNIN_NEXT_BUTTON =  "idSIButton9";

	private static final String LOCATOR_SIGNIN_PICK_ACCOUNT = "//div[contains(text(),'@massey')]";

	private static final String LOCATOR_USER_MENU			= "//div[@class='userControl dropdown-toggle ng-binding']";

	private static final String LOCATOR_SIGNOUT_USER		= "//div[@class='userControl dropdown-toggle ng-binding']/following-sibling::ul/li";

	private static final String LOCATOR_EXT_PASSWORD_FIELD = "passwordInput";

	private static final String LOCATOR_EXT_SIGN_IN_BUTTON = "submitButton";


	//---------------------------TEST USERS--------------------------------------------
	private static final String stester1User		= System.getenv("loginUserName");
	public  static final String stester1Password	= System.getenv("loginPassword");
	//-------------------------------------------------------------------------------------

	private static boolean isSignInEmailFieldDisplayed = false;
	private static boolean isPickAccountDisplayed = false;
	private static boolean isLoginNameDisplayed = false;


	private String firstPeriodOptionFile = "firstPeriodOptionFile.txt";


	@FindBy(xpath=LOCATOR_USER_MENU)
	private WebElement userMenu;

	@FindBy(id=LOCATOR_EXT_PASSWORD_FIELD)
	private WebElement extPasswordField;

	@FindBy(id=LOCATOR_EXT_SIGN_IN_BUTTON)
	private WebElement extSignInButton;

    protected WebDriver driver;


	/**
	 * Construct a pageobject and open the URL provided
	 * @param driver The Selenium webdriver object to attach to this pageobject, providing access to browser interface
	 * @throws FailedPreconditionException When the tests on opening the browser page fail.  These tests usually concern
	 * 		testing that we're really on the right webpage for this pageobject
	 */
    public JupiterBasePageObject(WebDriver driver) throws FailedPreconditionException
    {
    	//	this.switchToFrame(getDefaultFrame());
    		this.driver = driver;
    		PageFactory.initElements(driver, this);
    		this.checkValidPreconditions();		
    		
    		String currentDirectory = System.getProperty("user.dir");
    		//System.out.println("This is the current directory: " + currentDirectory);  		   		   		 		
    }
	

	/**
	 * Switch to a named frame within the web page.  All lookup's of interface components on the page by Selenium are
	 * in the context of a frame when the webpage uses frames 
	 * @param frame Name of the frame to make current
	 */
	public void switchToFrame(String frame) {
		// get out of the current frame
		driver.switchTo().window(driver.getWindowHandle());
//		this.waitForFrame(_frame,5);
		driver.switchTo().frame(frame);
	}
	
	/** 
	 * Get the instance of the webdriver attached to this object
	 * @return Selenium WebDriver object associated with this pageobject  
	 */
	public void setDriver(WebDriver setdriver) 
	{
		 this.driver = setdriver;
	}
	
	public WebDriver getDriver() 
	{
		 return this.driver;
	}
	
	public String getStester1Password() 
	{
		 return this.stester1Password;
	}
	
	public String getFirstPeriodOptionFileName()
	{
		return this.firstPeriodOptionFile;
	}
	
	
	/**
	 * gets the default timeout (seconds) to be used when waiting for items to become visible on a page 
	 * @return timeout
	 */
	public int getWaitTimeout() {
		return _waitTimeout;
	}
	
	/**
	 * sets the default timeout (seconds) to be used when waiting for items to become visible on a page
	 * @param seconds Number of seconds 
	 */
	public void setWaitTimeout(int seconds) {
		_waitTimeout = seconds;
	}
	
	/**
	 * Set the implicit wait mode for this web driver.  This will take effect for all pages and objects from this
	 * point on for this webdriver instance.  Use with care, as it can slow tests down if the object you are looking
	 * for is not on the current page.  Does not affect waiting for a page to load, which is controlled by the page load timeout 
	 * @param seconds Number of seconds to wait before failing when looking up an object/frame on the current page with
	 * Selenium.
	 */
	public void setGlobalImplicitWait(int seconds) {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
		
	}
	
	/**
	 * Set the global page load timeout for Selenium.  Should be set to a number that is high enough so that tests do not
	 * fail just because page loads are a little bit slow, but also not to such a high value that tests are held up for
	 * too long if a page load does fail. 
	 * @param seconds
	 */
	public void setGlobalPageLoadWait(int seconds) {
		driver.manage().timeouts().pageLoadTimeout(seconds, TimeUnit.SECONDS);
	}
	
	/**
	 * Get default frame name
	 * @return The name of the default frame within this webpage.  Used to pre-set which frame we are on when constructing
	 * the page for the first time   
	 */
	public String getDefaultFrame() {
		return "top_frame";
	}
	
	/**
	 * Log-out the current user
	 * @throws InterruptedException 
	 */
		
	
	public boolean isSignInEmailFieldDisplayed()
	{
		isSignInEmailFieldDisplayed = driver.findElement(By.xpath(LOCATOR_SIGNIN_EMAIL_FIELD)).isDisplayed();
		
		 return isSignInEmailFieldDisplayed;		 
	}
	
	public boolean isPickAccountDisplayed()
	{
		isPickAccountDisplayed = driver.findElement(By.xpath(LOCATOR_SIGNIN_PICK_ACCOUNT)).isDisplayed();
		
		 return isPickAccountDisplayed;		 
	}
	
	public void signInCheck(String s) throws Exception
	{		
		       		
		try 
		{
			
			WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
			WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_SIGNIN_EMAIL_FIELD)));
			
			isSignInEmailFieldDisplayed = driver.findElement(By.id(LOCATOR_SIGNIN_EMAIL_FIELD)).isDisplayed();
						
		} 
		catch (Exception e) 
		{
			isSignInEmailFieldDisplayed = false;
			e.printStackTrace();
		}
		
		if(isSignInEmailFieldDisplayed==true)
		{		
			signInUser(s);						
		}
		else if(isSignInEmailFieldDisplayed==false)
		{
			System.out.println("------------ATTEMPT TO CLICK AN ACCOUNT-------------------");
			try 
			{
				isPickAccountDisplayed = driver.findElement(By.xpath(LOCATOR_SIGNIN_PICK_ACCOUNT)).isDisplayed();
				System.out.println("------------IS PICK ACCOUNT DISPLAYED: " + isPickAccountDisplayed + " -------------------");
				
			} catch (Exception e) 
			{
				isPickAccountDisplayed = false;
				e.printStackTrace();
			}
			
			if(isPickAccountDisplayed == true)
			{				
				driver.findElement(By.xpath(LOCATOR_SIGNIN_PICK_ACCOUNT)).click();
				Thread.sleep(4000);
				
				String currentTitle = driver.getTitle();
				System.out.println("The current Title is: " + currentTitle);				
												
				if(currentTitle.contains("adfs.massey"))
				{
					signInUser(s);
				}
			}
			else
				System.out.println("------------UNABLE TO CLICK THE ACCOUNT ! User has either automatically signed in or has ended up in the wrong page. -------------------");
		}
		
			
	}
	
	public void signInUser(String userProfile) throws AWTException, InterruptedException
	{
		
			String userEmail = "";
			String password = "";
					
			if(userProfile.contains("stester1"))
			{
				userEmail = stester1User;
				password  = stester1Password;
			}
			else
				System.out.println("No matching profiles found !");
							
			
			WebDriverWait wait4 = new WebDriverWait(driver, driverWaitInSec);
			
			WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
			WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_SIGNIN_EMAIL_FIELD)));
			
			WebDriverWait wait1 = new WebDriverWait(driver, driverWaitInSec);
			WebElement element1_1 = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_SIGNIN_NEXT_BUTTON)));
			
					
			driver.findElement(By.id(LOCATOR_SIGNIN_EMAIL_FIELD)).sendKeys(userEmail);
			
			wait4.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='lightbox-cover']")));
			
			driver.findElement(By.id(LOCATOR_SIGNIN_NEXT_BUTTON)).click();
			
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_EXT_PASSWORD_FIELD)));
			
			extPasswordField.sendKeys(password);
			
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_EXT_SIGN_IN_BUTTON)));
			
			extSignInButton.click();
			
			wait4.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='lightbox-cover']")));
			
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_SIGNIN_NEXT_BUTTON)));			
			driver.findElement(By.id(LOCATOR_SIGNIN_NEXT_BUTTON)).click();
								
		
	}
	
	
	public boolean isAdminPageDisplayed()
	{
		boolean adminPageDisplayed = false;
		
		try 
		{
			WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
			WebElement element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_BULKMAINTENANCE_MENUITEM)));
							
			adminPageDisplayed = element1.isDisplayed();
		} 
		catch(Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}	
		
		if(adminPageDisplayed == true)
		{
			return true;
		}
		else
			return false;	
	}
	
	public void typeInField(String xpath, String value) throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		
		Actions builder = new Actions(driver);
        
        //Generating an action to type a text		
		Action typeInCAPS = builder.sendKeys(element, value ).build();
	
		//Performing the typeInCAPS action
		typeInCAPS.perform();

	}
	
	public void typeInFieldWithExistingText(String xpath, Keys value)
	{
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		
		Actions builder = new Actions(driver);
        
        //Generating an action to type a text		
		Action typeInCAPS = builder.sendKeys(element, value ).build();
    
		//Performing the typeInCAPS action
		typeInCAPS.perform();

	}
	
	
	
	
	public void clickMenuButton()
	{
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_MENU_BUTTON)));
		
		element.click();
		
	}
	
	public void clickMenuItem(String menuItem)
	{
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@class,'menuItem') and contains(.,'" + menuItem + "')]")));
		
		element.click();
		
	}
	
	public boolean isCorrectMenuItemsDisplayed(String accessType)
	{
		boolean isCorrectMenuItemsDisplayed = false;
		int numFoundMenuItems = 0;
		
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@class,'menuItem')]")));
		
		
		int totalMenuItemListSize = driver.findElements(By.xpath("//*[contains(@class,'menuItem')]")).size();
		System.out.println("MENU ITEM LIST SIZE IS " + totalMenuItemListSize);
		
		List<WebElement> menuItemList = driver.findElements(By.xpath("//*[contains(@class,'menuItem')]"));
	
		
		if(accessType.equalsIgnoreCase("Edit Events"))
		{
			
			//loop through the list of menu items to find a match 
			for(WebElement i:menuItemList)
			{
				System.out.println("Menu Item found in list: " + i);
				if(i.getText().contains("Events"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}				
			
			}
			
			if(numFoundMenuItems == 1 && totalMenuItemListSize == 1)
			{
				System.out.println("We have found only 1 single menu item of Events for a user with Edit Events permissions");
				isCorrectMenuItemsDisplayed = true;
			}
			
			
			
		}
		else if(accessType.equalsIgnoreCase("Edit App Content"))
		{
			
			//loop through the list of menu items to find a match 
			for(WebElement i:menuItemList)
			{				
				System.out.println("Menu Item found in list: " + i);
				if(i.getText().contains("Campus Details"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Map Points"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Events"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Student Services"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Contacts"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Orientation"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
			}
			
			if(numFoundMenuItems == 6 && totalMenuItemListSize == 6)
			{
				System.out.println("We have found 6 menu items for a user with Edit App Content permissions");
				isCorrectMenuItemsDisplayed = true;
			}
			
			
		}
		else if(accessType.equalsIgnoreCase("Edit Map Points"))
		{
		
			//loop through the list of menu items to find a match 
			for(WebElement i:menuItemList)
			{				
				System.out.println("Menu Item found in list: " + i);
				
				if(i.getText().contains("Map Points"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				
			}
			
			if(numFoundMenuItems == 1 && totalMenuItemListSize == 1)
			{
				System.out.println("We have found only 1 single menu item of Map Points for a user with Edit Map Points permissions");
				isCorrectMenuItemsDisplayed = true;
			}
			
			
		}
		else if(accessType.equalsIgnoreCase("Edit All"))
		{
			
			//loop through the list of menu items to find a match 
			for(WebElement i:menuItemList)
			{				
				System.out.println("Menu Item found in list: " + i);
				if(i.getText().contains("Campus Details"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Help Items"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Map Points"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Map Point Types"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Map Point Groups"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Events"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Event Categories"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Rec Centre"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Student Services"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Contacts"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
				else if(i.getText().contains("Orientation"))
				{
					System.out.println("Menu Item " + i.getText() + " is matched.");
					numFoundMenuItems++;				
				}
			}
			
			if(numFoundMenuItems == 11 && totalMenuItemListSize == 11)
			{
				System.out.println("We have found 11 menu items for a user with Edit All permissions");
				isCorrectMenuItemsDisplayed = true;
			}
			
		}
		
		
		return isCorrectMenuItemsDisplayed;
	}
	
	public void clickButton(String xpath)
	{
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		
		Actions action = new Actions(driver);
		action.moveToElement(element).click().perform();
		
	}
	
	public void clickSaveButton()
	{
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_SAVE_BUTTON)));
		
		saveButton.click();
		
	}
	
	public void clickOKButton()
	{
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_OK_BUTTON)));
		
		okButton.click();
		
	}
	
	public void clickDeleteButton()
	{
		
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_DELETE_BUTTON)));		
		
		deleteButton.click();
		
	}
	
		
	
	public void searchForARecord(String recordName) throws InterruptedException
	{
		Thread.sleep(1500);
		
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("Ed-loading")));	
		
		clearSearchField();
		
		WebDriverWait wait1 = new WebDriverWait(driver, driverWaitInSec);
		WebElement element1 = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_SEARCH_FIELD)));		
		
		typeInField(LOCATOR_SEARCH_FIELD,recordName);
		
		//element.sendKeys(recordName);
		Thread.sleep(1500);
		
		
	}
	
	
	public void clearSearchField()
	{
		WebDriverWait wait = new WebDriverWait(driver, driverWaitInSec);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_SEARCH_FIELD)));		
		
		//element.clear();
		element.click();
		element.sendKeys(Keys.chord(Keys.CONTROL,"a"));
		element.sendKeys(Keys.BACK_SPACE);
	}
	
					
	
	
	
	
	
	
	
	
			
				
	
	/**
	 * Check preconditions for the pageobject (usually just during construction)
	 * @throws FailedPreconditionException When the page appears to be the wrong one to be handled by this pageobject
	 * or if some other failure condition happens 
	 */
	protected void checkValidPreconditions() throws FailedPreconditionException {
		if (!onCorrectPage()) {
			throw new FailedPreconditionException("Browser page contents are not appropriate for this page object");
		}
	}
	
	/**
	 * Check whether the contents of the current browser page are appropriate for the current page object (eg. a login
	 * page object actually sees components on the browser page that look like a login page) 
	 * @return True if the page is appropriate for this pageobject 
	 */
	public abstract boolean onCorrectPage();
	
	/**
	 * Wait for a named item to become available on the web page.  Used when navigating between pages, waiting for the new
	 * page to load.  Also can be used if ajax were to expose a dynamic element on the page
	 * @return Did the item become visible (boolean)
	 * @param itemName Name of element to wait for
	 * @param seconds How many seconds to wait for the frame to become available   
	 */
	public boolean waitForNamedItem(String itemName, int seconds) {
		Wait<WebDriver> wait = new WebDriverWait(driver, seconds);         
		return wait.until(new OptionAvailable(driver,By.name(itemName))).isEnabled();      
	}

	/**
	 * Wait for a item with a certain ID to become available on the web page.  Used when navigating between pages, waiting for the new
	 * page to load.  Also can be used if ajax were to expose a dynamic element on the page
	 * @return Did the item become visible (boolean)
	 * @param itemID ID attribute of element to wait for
	 * @param seconds How many seconds to wait for the frame to become available   
	 */
	public boolean waitForIdItem(String itemID, int seconds) {
		Wait<WebDriver> wait = new WebDriverWait(driver, seconds);
		try {
			return wait.until(new OptionAvailable(driver,By.id(itemID))).isEnabled();
		}
		catch (Exception e) {
			return false;
		}
	}

	/**
	 * Wait for a item with a certain xpath to become available on the web page.  Used when navigating between pages, waiting for the new
	 * page to load.  Also can be used if ajax were to expose a dynamic element on the page
	 * @return Did the item become visible (boolean)
	 * @param xpath xpath describing element to wait for
	 * @param seconds How many seconds to wait for the frame to become available   
	 */
	public boolean waitForXPathItem(String xpath, int seconds) {
		Wait<WebDriver> wait = new WebDriverWait(driver, seconds);
		try {
			return wait.until(new OptionAvailable(driver,By.xpath(xpath))).isEnabled();      
		}
		catch (Exception e) {
			return false;
		}
	}

	/**
	 * Wait for a item with a certain CSS descriptor to become available on the web page.  Used when navigating between pages, waiting for the new
	 * page to load.  Also can be used if ajax were to expose a dynamic element on the page
	 * @return Did the item become visible (boolean)
	 * @param css CSS describing element to wait for
	 * @param seconds How many seconds to wait for the frame to become available   
	 */
	public boolean waitForCSSItem(String css, int seconds) {
		Wait<WebDriver> wait = new WebDriverWait(driver, seconds);
		try {
			return wait.until(new OptionAvailable(driver,By.cssSelector(css))).isEnabled();      
		}
		catch (Exception e) {
			return false;
		}
	}
	
	/** 
	 * Waits until the option targeted by "byOption" is available in a select list       
	 * @param select Parent element of the option to wait for (the element that attaches to the select list itself)      
	 * @param byOption Child item to look for.  Can pass a "By" option that will look by name, id, xpath, or css.
	 * @param seconds How long to wait for the item to become visible
	 * @return Did the select option become visible within the allowed time?
	 */     
	public boolean waitForOption(WebElement select, By byOption, int seconds) {         
		Wait<WebDriver> wait = new WebDriverWait(driver, seconds);
		try {
			return wait.until(new OptionAvailable(select, byOption)).isEnabled();      
		}
		catch (Exception e) {
			return false;
		}
	}      
	
	public static class OptionAvailable             
	implements ExpectedCondition<WebElement> {         
		private WebElement select = null;         
		private By byOption;   
		private WebDriver _driver = null;
		
		public OptionAvailable(WebElement select, By byOption) {
			this.select = select;             
			this.byOption = byOption;         
		}          

		public OptionAvailable(WebDriver driver, By byOption) {
			this.select = null;   
			this._driver = driver; 
			this.byOption = byOption;         
		}          

		@Override         
		public WebElement apply(WebDriver input) {
			if (select == null)
				return _driver.findElement(byOption);
			else
				return select.findElement(byOption);         
		}     
	}
	

	
	

	
	

	
	
	
}
	
