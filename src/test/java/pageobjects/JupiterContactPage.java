package pageobjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobject.core.BasePageObject;
import pageobject.core.FailedPreconditionException;


public class JupiterContactPage extends BasePageObject
{



	//--------------------------Add new homepage elements--------------------------------------------------------------




	private static final String LOCATOR_FORENAME_TEXT = "//input[@type='text' and @name='forename']";
	@FindBy(xpath=LOCATOR_FORENAME_TEXT)
	private WebElement forenameTextBox;

	private static final String LOCATOR_SURNAME_TEXT = "//input[@type='text' and @name='surname']";
	@FindBy(xpath=LOCATOR_SURNAME_TEXT)
	private WebElement surnameTextBox;

	private static final String LOCATOR_EMAIL_TEXT = "//input[@type='email' and @name='email']";
	@FindBy(xpath=LOCATOR_EMAIL_TEXT)
	private WebElement emailTextBox;

	private static final String LOCATOR_TELEPHONE_NUMBER = "//input[@type='tel' and @name='telephone']";
	@FindBy(xpath=LOCATOR_TELEPHONE_NUMBER)
	private WebElement telephoneTextBox;

	private static final String LOCATOR_MESSAGE_TEXT = "//textarea[@type='text' and @name='message']";
	@FindBy(xpath=LOCATOR_MESSAGE_TEXT)
	private WebElement messageTextBox;

	private static final String LOCATOR_SUBMIT_BUTTON = ".//a[contains(text(),'Submit')]";
	@FindBy(xpath=LOCATOR_SUBMIT_BUTTON)
	private WebElement submitButtonItem;

//	private static final String LOCATOR_SUCCESS_MESSAGE = "//*[contains(text(),'we appreciate your feedback')]";
//	private static final String LOCATOR_SUCCESS_MESSAGE = ".//div[contains(text(),' we appreciate your feedback')]";
	private static final String LOCATOR_SUCCESS_MESSAGE = ".//div[starts-with(@class,'alert')]";
	@FindBy(xpath=LOCATOR_SUCCESS_MESSAGE)
	private WebElement successMessageItem;


	private static final String LOCATOR_SHOP_LINK = "//span[text() = 'Shop']";
	@FindBy(xpath=LOCATOR_SHOP_LINK)
	private WebElement shopLinkItem;

	private static final String LOCATOR_BACK_BUTTON = ".//a[contains(text(),'Back')]";
	@FindBy(xpath=LOCATOR_BACK_BUTTON)
	private WebElement backButtonItem;


//
//	<div class="alert alert-success">
//		<strong class="ng-binding">Thanks Pranay</strong>, we appreciate your feedback.
//	</div>
	//---------------------------------------------------------------------------------------------------------------------


	private WebElement pageTitle;



	public JupiterContactPage(WebDriver driver)
			throws FailedPreconditionException {
		// TODO Auto-generated constructor stub
		super(driver);
	}

	//@Override
	public boolean onCorrectPage() 
	{
		// Enter Account Information
		
		try 
		{
			return true;
		}
		catch (Exception e) 
		{
			return false;
		}
		
	}
		/**
		 * Get the title of the current page
		 * @return Page title
		 */
	public String getPageTitle() {
		return pageTitle.getText();
	}

	public void enterforenameTextBox(String Forename)
	{
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_FORENAME_TEXT)));

		forenameTextBox.sendKeys(Forename);

	}

	public void enterSurnameTextBox(String Surname)
	{
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_SURNAME_TEXT)));

		surnameTextBox.sendKeys(Surname);

	}

	public void enterEmailTextBox(String eMail)
	{
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_EMAIL_TEXT)));

		emailTextBox.sendKeys(eMail);

	}

	public void enterPhoneTextBox(String PhoneNumber)
	{
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_TELEPHONE_NUMBER)));

		telephoneTextBox.sendKeys(PhoneNumber);

	}


	public void enterMessageTextBox(String Message)
	{
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_MESSAGE_TEXT)));

		messageTextBox.sendKeys(Message);

	}

	public void clickSubmitTextBox()
	{
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_SUBMIT_BUTTON)));
		submitButtonItem.click();

	}


}

