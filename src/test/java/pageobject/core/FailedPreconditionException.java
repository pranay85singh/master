package pageobject.core;

public class FailedPreconditionException extends Exception {

	public FailedPreconditionException(String message) {
		super(message);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 552428815573769813L;
}
