package pageobjects;



import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import pageobject.core.BasePageObject;
import pageobject.core.FailedPreconditionException;
import test.core.BaseTest;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import test.core.TestUtil;


public class ReportPage extends BasePageObject
{
	
	
	
	//--------------------------Add new manageit reports elements--------------------------------------------------------------
	
	
	
	private static final String LOCATOR_MANAGEIT_REPORTS_HEADER = "//h2[text() = 'ManageIT Reports']";    
	@FindBy(xpath=LOCATOR_MANAGEIT_REPORTS_HEADER)
	private WebElement manageITReportHeader;
	
	private static final String LOCATOR_REPORT_CATEGORY_LEDGER_ACCOUNT_REPORTS = "//a[@name = 'Ledger Account Reports']";    
	@FindBy(xpath=LOCATOR_REPORT_CATEGORY_LEDGER_ACCOUNT_REPORTS)
	private WebElement reportCategoryLedgerAccountReports;
	
	
	private static final String LOCATOR_REPORT_CHARGES_BY_BUDGET_CENTRE = "//h4[text() = 'Charges by Budget Centre']";    
	@FindBy(xpath=LOCATOR_REPORT_CHARGES_BY_BUDGET_CENTRE)
	private WebElement reportChargesByBudgetCentre;
	
	private static final String LOCATOR_REPORT_CHARGES_BY_BUDGET_CENTRE_LINK = "//a[@name = 'Charges by Budget Centre' and @href = 'https://manageitreports-uat.massey.ac.nz/Reports_ManageIT/report/General User/1000']/child::h4[text() = 'Charges by Budget Centre']";
	@FindBy(xpath=LOCATOR_REPORT_CHARGES_BY_BUDGET_CENTRE_LINK)
	private WebElement reportChargesByBudgetCentreLink;

	private static final String LOADING_PAGE_VARIABLE =	"//*[contains(@id,'ReportViewerControl_AsyncWait_Wait')]";
	@FindBy(xpath=LOADING_PAGE_VARIABLE)
	private WebElement reportLoadingPage;

//	private static final String LOCATOR_SUCCESS_MESSAGE = ".//div[contains(text(),'we appreciate your feedback.')]";
	private static final String LOCATOR_SUCCESS_MESSAGE = "//div[2]/div/div";
	@FindBy(xpath=LOCATOR_SUCCESS_MESSAGE)
	private WebElement successMessageItem;

	private static final String LOCATOR_ERROR_FORENAME = ".//span[contains(text(),'Forename is required')]";
	@FindBy(xpath=LOCATOR_ERROR_FORENAME)
	private WebElement forenameErrorItem;

	private static final String LOCATOR_ERROR_EMAIL = ".//span[contains(text(),'Email is required')]";
	@FindBy(xpath=LOCATOR_ERROR_EMAIL)
	private WebElement emailErrorItem;

	private static final String LOCATOR_ERROR_MESSAGE = ".//span[contains(text(),'Message is required')]";
	@FindBy(xpath=LOCATOR_ERROR_MESSAGE)
	private WebElement messageErrorItem;


	private static final String LOCATOR_INVALID_EMAIL = ".//span[contains(text(),'Please enter a valid email')]";
	@FindBy(xpath=LOCATOR_INVALID_EMAIL)
	private WebElement invalidEmailItem;

	private static final String LOCATOR_INVALID_PHONENUMBER = ".//span[contains(text(),'Please enter a valid telephone number')]";
	@FindBy(xpath=LOCATOR_INVALID_PHONENUMBER)
	private WebElement invalidPhoneNumberItem;

	private static final String LOCATOR_BACK_BUTTON = ".//a[contains(text(),'Back')]";
	@FindBy(xpath=LOCATOR_BACK_BUTTON)
	private WebElement backButtonItem;



//---------------------------------------------------------------------------------------------------------------------	
		
	
	
	
	public ReportPage(WebDriver driver) throws FailedPreconditionException 
	{
		// TODO Auto-generated constructor stub
		super(driver);
		
		String currentDirectory = System.getProperty("user.dir");
		System.out.println("This is the current directory: " + currentDirectory);
		

		
	}

	//@Override
	public boolean onCorrectPage() 
	{
		// Enter Account Information
			
		try 
		{
			return true;
		}
		catch (Exception e) 
		{
			return false;
		}
			
	}
	
	
	
	
	
	
	public boolean isManageITReportPageDisplayed()
	{
		
		boolean isManageITReportPageDisplayed = false;
		
		try 
		{
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(LOADING_PAGE_VARIABLE)));
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_MANAGEIT_REPORTS_HEADER)));
							
			isManageITReportPageDisplayed = manageITReportHeader.isDisplayed();
			driver.navigate().refresh();
		} 
		catch(Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}	
		
		if(isManageITReportPageDisplayed == true)
		{
			return true;
		}
		else
			return false;	
		
		
	}
	
	
	public void clickReportCategoryLedgerAccountReports()
	{
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_REPORT_CATEGORY_LEDGER_ACCOUNT_REPORTS)));
		reportCategoryLedgerAccountReports.click();
	}
	
	
	public boolean isCorrectReportChargesByBudgetCentreLinkDisplayed()
	{
		
		boolean isCorrectReportChargesByBudgetCentreLinkDisplayed;
		
		try 
		{
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_REPORT_CHARGES_BY_BUDGET_CENTRE_LINK)));
							
			isCorrectReportChargesByBudgetCentreLinkDisplayed = reportChargesByBudgetCentreLink.isDisplayed();
		} 
		catch(Exception e) 
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return false;
		}	
		
		if(isCorrectReportChargesByBudgetCentreLinkDisplayed == true)
		{
			return true;
		}
		else
			return false;	
		
		
	}


	public boolean isCorrectSuccessMessageDisplayed()
	{

		boolean isCorrectSuccessMessageDisplayed;

		try
		{

			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_BACK_BUTTON)));
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_SUCCESS_MESSAGE)));

			isCorrectSuccessMessageDisplayed = successMessageItem.isDisplayed();
		}
		catch(Exception e)
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return false;
		}

		if(isCorrectSuccessMessageDisplayed == true)
		{
			return true;
		}
		else
			return false;


	}


	public boolean isErrorMessageDisplayed()
	{

		boolean isErrorForemnameDisplayed;
		boolean isErrorEmailDisplayed;
		boolean isErrorMessageDisplayed;

		try
		{
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_ERROR_FORENAME)));
			isErrorForemnameDisplayed = forenameErrorItem.isDisplayed();
		}
		catch(Exception e)
		{
			TestUtil.reportLog("****   Forename is not a mandatory field. ****");
			return false;
		}

		try
		{
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_ERROR_EMAIL)));
			isErrorEmailDisplayed = emailErrorItem.isDisplayed();
		}
		catch(Exception e)
		{
			TestUtil.reportLog("****   Email is not a mandatory field. ****");
			return false;
		}

		try
		{
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_ERROR_MESSAGE)));
			isErrorMessageDisplayed = messageErrorItem.isDisplayed();
		}
		catch(Exception e)
		{
			TestUtil.reportLog("****   Message is not a mandatory field. ****");
			return false;
		}

		if(isErrorForemnameDisplayed && isErrorEmailDisplayed && isErrorMessageDisplayed)
		{
			return true;
		}
		else
			return false;


	}


	public boolean isErrorForInvalidFormatDisplayed()
	{

		boolean isErrorInvalidEmailDisplayed;
		boolean isErrorInvalidPhoneNDisplayed;

		try
		{
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_INVALID_EMAIL)));
			isErrorInvalidEmailDisplayed = invalidEmailItem.isDisplayed();
		}
		catch(Exception e)
		{
			TestUtil.reportLog("****   Unable to find Email Error Field. ****");
			return false;
		}

		try
		{
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_INVALID_PHONENUMBER)));
			isErrorInvalidPhoneNDisplayed = invalidPhoneNumberItem.isDisplayed();
		}
		catch(Exception e)
		{
			TestUtil.reportLog("****   Unable to find Phone Number Error field. ****");
			return false;
		}



		if(isErrorInvalidEmailDisplayed && isErrorInvalidPhoneNDisplayed)
		{
			return true;
		}
		else
			return false;


	}



	public boolean isErrorMessageGone()
	{

		try
		{
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(LOCATOR_ERROR_FORENAME)));
		}
		catch(Exception e)
		{
			TestUtil.reportLog("****   Forename is not a mandatory field. ****");
			return false;
		}

		try
		{
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(LOCATOR_ERROR_EMAIL)));
		}
		catch(Exception e)
		{
			TestUtil.reportLog("****   Email is not a mandatory field. ****");
			return false;
		}

		try
		{
			new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(LOCATOR_ERROR_MESSAGE)));
		}
		catch(Exception e)
		{
			TestUtil.reportLog("****   Message is not a mandatory field. ****");
			return false;
		}

		return true;


	}


	
	
}

