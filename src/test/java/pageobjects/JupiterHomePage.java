package pageobjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobject.core.BasePageObject;
import pageobject.core.FailedPreconditionException;



public class JupiterHomePage extends BasePageObject
{



	//--------------------------Add new homepage elements--------------------------------------------------------------




	private static final String LOCATOR_CONATCT_LINK = "//a[contains(text(),'Contact')]";
	@FindBy(xpath=LOCATOR_CONATCT_LINK)
	private WebElement contactLinkItem;

	private static final String LOCATOR_SHOP_LINK = "//a[contains(text(),'Shop']";
	@FindBy(xpath=LOCATOR_SHOP_LINK)
	private WebElement shopLinkItem;

	//---------------------------------------------------------------------------------------------------------------------


	private WebElement pageTitle;



	public JupiterHomePage(WebDriver driver)
			throws FailedPreconditionException {
		// TODO Auto-generated constructor stub
		super(driver);
	}

	//@Override
	public boolean onCorrectPage() 
	{
		// Enter Account Information
		
		try 
		{
			return true;
		}
		catch (Exception e) 
		{
			return false;
		}
		
	}
		/**
		 * Get the title of the current page
		 * @return Page title
		 */
	public String getPageTitle() {
		return pageTitle.getText();
	}



	public void clickcontactLinkItem()
	{
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_CONATCT_LINK)));

		contactLinkItem.click();

	}

	public void clickLinkLinkItem()
	{
		new WebDriverWait(driver, driverWaitInSec).until(ExpectedConditions.elementToBeClickable(By.xpath(LOCATOR_SHOP_LINK)));

		shopLinkItem.click();

	}
	
	
}

