package test.core;

import java.io.IOException;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
 
public class TestStatusListener extends BaseTest implements ITestListener
{
	@Override 
	public void onTestStart(ITestResult result) 
	{
	// TODO Auto-generated method stub 
	}
	
	@Override
	public void onTestSuccess(ITestResult result) 
	{
		System.out.println("Passed verification point..taking screenshot"); 		
		TestUtil.takeScreenshot(result.getName().toString(),"Passed");		
	}
	
	@Override 
	public void onTestFailure(ITestResult result) 
	{
		System.out.println("Failed verification point..taking screenshot");	
		TestUtil.takeScreenshot(result.getName().toString(),"Failed"); 
	}
	 
	@Override 
	public void onTestSkipped(ITestResult result) 
	{
	// TODO Auto-generated method stub	 
	}
	
	@Override  
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) 
	{
	// TODO Auto-generated method stub	 
	}
	
	@Override  
	public void onStart(ITestContext context) 
	{
	// TODO Auto-generated method stub	 
	}
	
	@Override 
	public void onFinish(ITestContext context) 
	{
	// TODO Auto-generated method stub	 
	}
	
 
}